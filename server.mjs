import express from 'express'
import bodyParser from 'body-parser'
import path from 'path'
import config from './config'
import api from './api'
import './helpers/db'

const __dirname = path.dirname(new URL(import.meta.url).pathname)
const port = config.apiPort || 4243
const app = express()

app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, 'dist')))
app.use('/api', api)

app.get('*', (req, res) =>
    res.sendFile(path.join(__dirname, 'dist/index.html')))

app.listen(port, () => console.log(`Started on port ${port}`))
