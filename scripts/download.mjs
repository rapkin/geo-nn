import cfg from '../config'
import axios from 'axios'
import mkdirp from 'mkdirp'
import path from 'path'
import fs from 'fs'
import _ from 'lodash'
import { lon2tile, lat2tile } from '../helpers/common'
import { searchCities } from '../helpers/overpass'
const tilesDir = path.join(cfg.workingDir, 'tiles')


const getTileLink = (x, y, zoom) =>
    `https://khms1.googleapis.com/kh?v=800&hl=uk-UA&x=${x}&y=${y}&z=${zoom}`


async function loadTile(tile, file) {
    const s = fs.createWriteStream(file)
    const load = async (retry) => {
        try {
            await axios({
                method: 'get',
                responseType: 'stream',
                url: tile
            }).then((res) => res.data.pipe(s))
            return true
        } catch(e) {
            console.log('Failed to load', tile, 'retry')
            if (retry) {
                console.error(e)
                return false
            } else return load(true)
        }
    }
    return await load()
}


async function downloadTiles(bounds, zoom = cfg.zoom) {
    mkdirp.sync(tilesDir)
    const minX = lon2tile(bounds.minlon, zoom)
    const minY = lat2tile(bounds.minlat, zoom)
    const maxX = lon2tile(bounds.maxlon, zoom)
    const maxY = lat2tile(bounds.maxlat, zoom)

    const dx = Math.abs(minX - maxX)
    const fromX = Math.min(minX, maxX)
    const dy = Math.abs(minY - maxY)
    const fromY = Math.min(minY, maxY)

    const chunks = _.chunk(_.range(dx*dy).map(i => async () => {
        const _y = Math.floor(i / dx)
        const x = (i - _y*dx) + fromX
        const y = _y + fromY

        const tile = getTileLink(x, y, zoom)
        const file = `z${zoom}y${y}x${x}.png`
        const filePath = path.join(tilesDir, file)
        if (fs.existsSync(filePath)) {
            console.log('tile already loaded', file)
            return file
        }

        console.log('load tile', tile, '=>', file)
        await loadTile(tile, filePath)
        return file
    }), 8)

    for (let i in chunks) {
        const k = i*1 + 1
        const percent = Math.round(k / chunks.length * 100)
        console.log('CHUNK', k, '/', chunks.length, `${percent}%`)
        await Promise.all(chunks[i].map(f => f()))
    }
}


async function loadCity(name) {
    try {
        const cities = await searchCities(name)
        if (!cities || cities.length < 1) {
            console.warn('City', name, 'not found :(')
            return false
        }

        if (cities.length < 2) {
            const city = cities[0]
            await downloadTiles(city.bounds)
        }
        return true
    } catch(e) {
        console.error(e)
        return false
    }
}


loadCity('Рівне')
