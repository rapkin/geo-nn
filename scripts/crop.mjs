import '../helpers/db'
import cfg from '../config'
import mkdirp from 'mkdirp'
import sharp from 'sharp'
import path from 'path'
import rimraf from 'rimraf'
import _ from 'lodash'
import Cities from '../models/cities'
import Points from '../models/points'
import Labels from '../models/Labels'

const size = cfg.samplingSize
const tilesDir = path.join(cfg.workingDir, 'tiles')
const outDir = path.join(cfg.workingDir, 'samples')
rimraf.sync(outDir)

setTimeout(() => {
    console.error('Timeout error')
    process.exit()
}, 60000)

async function run() {
    const city = (await Cities.find())[0]
    const labels = await Labels.find()
    const points = await Points.find()

    const byGroups = _.groupBy(points, 'label')
    for (let labelId in byGroups) {
        const labelDir = path.join(outDir, labelId)
        mkdirp.sync(labelDir)

        const group = byGroups[labelId]
        const label = labels.find(l => l._id.toString() === labelId.toString())

        const tasks = group.map(p => () => {
            const [left, top] = [p.x * size % 256, p.y * size % 256]
            const tileX = Math.floor(p.x * size / 256)
            const tileY = Math.floor(p.y * size / 256)
            const tile = path.join(tilesDir, `z${cfg.zoom}y${tileY}x${tileX}.png`)
            const outImg = path.join(labelDir, `${p.x}_${p.y}.png`)

            return new Promise((resolve, reject) =>{
                try {
                    sharp(tile)
                        .extract({ left, top, width: size, height: size })
                        .toFile(outImg, (err) => {
                            if (err) {
                                console.error('Failed to crop image', tile, '=>', outImg)
                                reject(err)
                            }
                            else resolve()
                        })
                } catch(e) {
                    console.error('Failed to crop image', tile, '=>', outImg)
                }
            })
        })

        try {
            for (let chunk of _.chunk(tasks, 10))
                await Promise.all(chunk.map(f => f()))
        } catch(e) {
            console.error('Error when crop', label.name, e)
        }
    }
    process.exit(0)
}

run()
