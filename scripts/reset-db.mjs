import '../helpers/db'
import db from '../models/initialDb'

import Cities from '../models/cities'
import Labels from '../models/labels'
import Points from '../models/points'

setTimeout(() => {
    console.error('Timeout error')
    process.exit(1)
}, 10000)

const relations = [
    [Cities, db.cities],
    [Labels, db.labels],
    [Points, db.points]
]

const insert = (model, data) =>
    new Promise((resolve, reject) => {
        model.collection.drop((err, res) => {
            if (err) reject(err)
            else {
                if (data.length > 0) model.collection.insertMany(data, (err, res) => {
                    if (err) reject(err)
                    else resolve(res)
                })
                else resolve({skip: true})
            }
        })
    })

async function main() {
    try {
        for (let [model, data] of relations) {
            console.log('Insert', data.length, 'items to', model.collection.collectionName)
            await insert(model, data)
        }
        console.log('Import finished!')
        process.exit(0)
    } catch(e) {
        console.log('Import failed', e)
        process.exit(1)
    }
}

setTimeout(main, 1000)
