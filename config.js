const path = require('path')

module.exports = {
    apiPort: 2323,
    zoom: 19,
    samplingSize: 128,
    workingDir: path.join(__dirname, 'data'),
    overpassApi: 'https://overpass-api.de/api/interpreter'
}
