import cfg from '../config'
import axios from 'axios'
import qs from 'qs'


export const getHousesScipt = (bounds) => `
    [out:json];
    (
      node(${bounds.minlat}, ${bounds.minlon}, ${bounds.maxlat}, ${bounds.maxlon});
      <;
    );
    out bb;
`


export const overpassCall = (q) =>
    axios.post(cfg.overpassApi, `data=${encodeURIComponent(q)}`, {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    })


const searchCityScript = (name) => `
    [timeout:2][out:json];
    rel
        ["place"="city"]
        ["name"="${name}"];
    out bb;
`


const getCityScript = (id) => `
    [out:json];
    rel(${id});
    out bb;
`


const getCityGeometryScript = (id) => `
    [out:json];
    rel(${id});
    out geom;
`


const wrapCall = async (call, retFirst = false) => {
    try {
        const res = await overpassCall(call)
        return retFirst ? res.data.elements[0] : res.data.elements
    } catch(e) {
        console.error(e)
        return null
    }
}

export const getCity = (id) =>
    wrapCall(getCityScript(id), true)

export const searchCities = (name) =>
    wrapCall(searchCityScript(name))
