import cfg from '../config'

export const getSamplingScale = (map) =>
    cfg.samplingSize / map.getZoomScale(cfg.zoom, map.getZoom())


export const lon2tile = (lon, zoom) =>
    Math.floor((lon + 180) / 360*Math.pow(2, zoom))


export const lat2tile = (lat, zoom) =>
    Math.floor((1 - Math.log(Math.tan(lat*Math.PI/180) + 1 / Math.cos(lat*Math.PI/180)) / Math.PI) / 2 * Math.pow(2, zoom))
