import mongoose from 'mongoose'
import config from '../config'

mongoose.Promise = Promise
mongoose
    .connect(config.mongo || 'mongodb://localhost/geo')
    .catch((e) => {
        console.error('Connection error', e)
        process.exit(0)
    })
