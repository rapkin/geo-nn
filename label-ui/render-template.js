import cityTmpl from './templates/city.html'
import citiesContainerTmpl from './templates/cities-container.html'
import labelTmpl from './templates/label.html'
import modeButtonTmpl from './templates/mode-button.html'
import appTmpl from './templates/app.html'

const root = document.getElementById('root')

export const modes = {
    paint: {
        name: 'Paint [p]',
        icon: 'paint-brush'
    },
    inspect: {
        name: 'Inspect [i]',
        icon: 'mouse-pointer'
    }
}

export const layers = {
    standart: {
        name: 'Normal [n]',
        icon: 'map'
    },
    satellite: {
        name: 'Satellite [g]',
        icon: 'globe'
    }
}

export const brushes = {
    s: {
        name: '1x1 [s]',
        icon: 'square'
    },
    m: {
        name: '2x2 [m]',
        icon: 'th-large'
    },
    l: {
        name: '3x3 [l]',
        icon: 'th'
    }
}


const modesHtml = Object.keys(modes).map(key =>
    modeButtonTmpl(
        Object.assign({
            attr: 'mode',
            value: key
        }, modes[key])
    )
).join('')


const layersHtml = Object.keys(layers).map(key =>
    modeButtonTmpl(
        Object.assign({
            attr: 'layer',
            value: key
        }, layers[key])
    )
).join('')


const brushesHtml = Object.keys(brushes).map(key =>
    modeButtonTmpl(
        Object.assign({
            attr: 'brush',
            value: key
        }, brushes[key])
    )
).join('')


const renderCities = (cities, currentCity) => {
    const citiesHtml = cities.map(c => cityTmpl(Object.assign({
        active: c._id === currentCity._id
    }, c))).join('')
    return citiesContainerTmpl({ citiesHtml })
}


export const rerenderCities = (cities, currentCity) => {
    const $container = document.querySelector('.cities-container')
    $container.outerHTML = renderCities(cities, currentCity)
}


export default ({ labels, cities, currentCity }) => {
    const citiesHtml = renderCities(cities, currentCity)

    const labelsHtml = [
        labels.map((l, i) =>
            labelTmpl(Object.assign({ button: i+1 }, l))
        ).join(''),
        labelTmpl({
            _id: 'remove',
            name: 'Очистити',
            color: 'red',
            button: 'c'
        })
    ].join('')

    root.innerHTML = appTmpl({
        citiesHtml,
        labelsHtml,
        modesHtml,
        layersHtml,
        brushesHtml
    })
}
