import cfg from '../config'
import { getSamplingScale } from '../helpers/common'

const colorsHash = {}
const getLabelColor = (id, labels) => {
    if (id === 'remove') return 'red'
    if (colorsHash[id]) return colorsHash[id]
    return colorsHash[id] = labels.find(l => l._id === id).color
}

export default ($canvas, map, data) => {
    const { city, labels, points, hoverPoints } = data
    const rect = map.getContainer().getBoundingClientRect()
    $canvas.width = rect.width
    $canvas.height = rect.height
    const g = $canvas.getContext('2d')

    const drawHLine = (y) => {
        g.moveTo(0, y)
        g.lineTo(rect.width, y)
    }

    const drawVLine = (x) => {
        g.moveTo(x, 0)
        g.lineTo(x, rect.height)
    }

    const ss = getSamplingScale(map)
    const mapPixelBounds = map.getPixelBounds()

    const { x: minx, y: miny } = mapPixelBounds.min
    const { x: maxx, y: maxy } = mapPixelBounds.max
    const [sx, sy] = [
        Math.floor(minx/ss) * ss,
        Math.floor(miny/ss) * ss
    ]
    const [ex, ey] = [
        Math.ceil(maxx/ss) * ss,
        Math.ceil(maxy/ss) * ss
    ]

    if (cfg.zoom - map.getZoom() < 2) {
        g.strokeStyle = '#fff'
        g.lineWidth = 0.5

        for (let x = sx; x < ex; x += ss)
            drawVLine(x - mapPixelBounds.min.x)

        for (let y = sy; y < ey; y += ss)
            drawHLine(y - mapPixelBounds.min.y)

        g.stroke()
    }

    const renderPoints = (points, border = false) => {
        points.forEach(p => {
            const [x, y] = [
                p.x * ss - mapPixelBounds.min.x,
                p.y * ss - mapPixelBounds.min.y
            ]
            g.fillStyle = getLabelColor(p.label, labels)
            g.fillRect(x, y, ss, ss)
        })
    }

    const visiblePoints = points.filter(p => sx <= p.x * ss < ex && sy <= p.y * ss < ey)
    renderPoints(visiblePoints)
    renderPoints(hoverPoints)
}
