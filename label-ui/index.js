import './css'
import cfg from '../config'
import axios from 'axios'
import L from 'leaflet'
import { getSamplingScale } from '../helpers/common'
import { searchCities } from '../helpers/overpass'
import renderTemplate, { rerenderCities } from './render-template'
import renderCanvasOnMap from './render-canvas'
L.Icon.Default.imagePath = '/leaflet/dist/images/'

let queue = []
const state = {
    mode: 'inspect',
    layer: 'satellite',
    brush: 'm',
    draw: false,
    hoverPoints: [],
    city: null,
    label: null,
    foundCities: []
}

const layers = {
    standart: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'),
    satellite: L.tileLayer('https://khms1.googleapis.com/kh?v=800&hl=uk-UA&x={x}&y={y}&z={z}')
}

const getName = (p) => p.x + '_' + p.y

const getBrushSize = () =>
    ({ s: 1, m: 3, l: 6 })[state.brush]

const handleClick = ($els, attr, fn) =>
    $els.forEach(el =>
        el.addEventListener('click', () =>
            fn(el.getAttribute(attr))
        )
    )

const switchButton = ($buttons, attr, value) =>
    $buttons.forEach(_el => {
        if (_el.getAttribute(attr) === value)
            _el.classList.add('active')
        else
            _el.classList.remove('active')
    })

const savePoints = () => {
    const toSave = queue.filter(p => p.label !== 'remove')
    const toRemove = queue.filter(p => p.label === 'remove')

    if (toSave.length > 0) axios.post('/api/points', toSave)
    if (toRemove.length > 0) axios.post('/api/points/clear', toRemove)
    queue = []
}

const savePoint = (point) => {
    const idx = queue.findIndex(p => p.x === point.x && p.y === point.y)
    if (~idx) queue[idx] = point
    else queue.push(point)
}

const clearPoint = (point) =>
    axios(`/api/points/clear/${point.x}/${point.y}/`)

async function run() {
    const points = []
    let { data: cities } = await axios('/api/cities')
    const { data: labels } = await axios('/api/labels')
    let lastCity = localStorage.lastCity || cities[0]._id
    let city = cities.find(c => c._id === lastCity)
    renderTemplate({ labels, cities, currentCity: city })

    const $canvas = document.getElementById('canvas')
    const $addCityPopup = document.getElementById('add-city-popup')
    const $modes = document.querySelectorAll('[data-mode]')
    const $layers = document.querySelectorAll('[data-layer]')
    const $labels = document.querySelectorAll('[data-label]')
    let $cities = document.querySelectorAll('[data-city]')
    const $brushes = document.querySelectorAll('[data-brush]')


    const getCityBounds = (city) => new L.LatLngBounds(
        new L.LatLng(city.minlat, city.minlon),
        new L.LatLng(city.maxlat, city.maxlon)
    )
    const getCityCenter = (city) => [
        localStorage[city._id + '_clat'] || (city.minlat + city.maxlat) / 2,
        localStorage[city._id + '_clng'] || (city.minlon + city.maxlon) / 2
    ]

    const bounds = getCityBounds(city)
    const [clat, clon] = getCityCenter(city)

    const map = L.map('map', {
        zoomControl: false,
        maxBounds: bounds,
        maxBoundsViscosity: 1.0,
        minZoom: cfg.zoom - 4,
    }).setView([clat, clon], localStorage.zoom || cfg.zoom)

    const moveMap = (dx, dy) => {
        if (state.mode === 'inspect') return
        const center = map.latLngToLayerPoint(map.getCenter())
        center.x += 100*dx
        center.y += 100*dy
        map.flyTo(map.layerPointToLatLng(center))
    }

    const renderCanvas = () =>
        renderCanvasOnMap($canvas, map, {
            city,
            labels,
            points,
            hoverPoints: !state.block && state.mode === 'paint' ? state.hoverPoints : []
        })

    const blockDraw = () => {
        state.block = true
        $canvas.classList.add('blocked')
    }

    const unblockDraw = () => {
        state.block = false
        $canvas.classList.remove('blocked')
    }

    const loadPoints = async () => {
        const {
            min: { x: minx, y: miny },
            max: { x: maxx, y: maxy }
        } = map.getPixelBounds()
        const t = (n) => Math.floor(n / getSamplingScale(map))
        const ranges = [t(minx), t(maxx), t(miny), t(maxy)].join('/')
        const { data } = await axios(`/api/points/${ranges}`)
        const names = {}
        points.forEach(p => names[getName(p)] = true)
        data.forEach(p =>
            !names[getName(p)] && points.push(p)
        )
        return true
    }

    let loadTimeout = null
    const loadAndRenderCanvas = (lazy = true) => {
        blockDraw()
        renderCanvas()
        const fn = async () => {
            const hasUpdates = await loadPoints()
            unblockDraw()
            if (hasUpdates) renderCanvas()
        }
        clearTimeout(loadTimeout)
        if (lazy) loadTimeout = setTimeout(fn, 200)
        else fn()
    }

    const updateCity = (active = state.city) => {
        city = cities.find(c => c._id === active)
        state.city = active
        localStorage.lastCity = active
        switchButton($cities, 'data-city', active)
        renderCanvas()
        const [lat, lon] = getCityCenter(city)
        map.setMaxBounds(getCityBounds(city))
        map.panTo(L.latLng(lat, lon))
    }

    const updateLabel = (active = state.label) => {
        state.label = active
        switchButton($labels, 'data-label', active)
    }

    const updateLayer = (active = state.layer) => {
        state.layer = active
        const other = active === 'standart' ? 'satellite' : 'standart'
        map.removeLayer(layers[other])
        layers[active].addTo(map)
        switchButton($layers, 'data-layer', active)
    }

    const updateMode = (active = state.mode) => {
        state.mode = active
        if (active === 'inspect') {
            state.hoverPoints = []
            $canvas.classList.add('invisible')
        } else $canvas.classList.remove('invisible')

        switchButton($modes, 'data-mode', active)
        renderCanvas()
    }

    const updateBrush = (active = state.brush) => {
        state.brush = active
        switchButton($brushes, 'data-brush', active)
    }

    const addNewCity = async (item) => {
        await axios.put('/api/cities', item)
        const res = await axios('/api/cities')
        cities = res.data
        rerenderCities(cities, city)
        $cities = document.querySelectorAll('[data-city]')
        handleClick($cities, 'data-city', updateCity)
        $addCityPopup.classList.remove('visible')
    }

    const renderFoundCities = () => {
        const $container = $addCityPopup.querySelector('.popup-items-list')
        $container.innerHTML = ''

        if (state.foundCities.length > 0)
            state.foundCities.forEach(c => {
                const el = document.createElement('div')
                el.className = 'popup-list-item city-item'
                el.innerHTML = '<i class="fa fa-plus"></i>' + c.name
                el.onclick = () => {
                    addNewCity(c)
                    el.remove()
                }
                $container.appendChild(el)
            })
        else {
            const el = document.createElement('div')
            el.className = 'popup-list-item'
            el.innerText = 'Nothing found'
            $container.appendChild(el)
        }
    }

    const addPoint = (point) => {
        const idx = points.findIndex(p => p.x === point.x && p.y === point.y)

        if (point.label !== 'remove') {
            if (~idx) points[idx] = point
            else points.push(point)
            savePoint(point)
        } else if (~idx) {
            points.splice(idx, 1)
            savePoint(point)
        }
    }

    const addPoints = (_points) =>
        _points.forEach(addPoint)

    const handleDraw = (e) => {
        if (state.block) return
        const size = getBrushSize()
        const ss = getSamplingScale(map)
        const { min: start } = map.getPixelBounds()
        const [x, y] = [
            e.offsetX + start.x,
            e.offsetY + start.y
        ]

        const active = []
        for (let i = size; i > 0; i--) {
            for (let j = size; j > 0; j--) {
                active.push({
                    label: state.label,
                    x: Math.round(x / ss - size / 2) + i - 1,
                    y: Math.round(y / ss - size / 2) + j - 1
                })
            }
        }
        state.hoverPoints = active
        if (state.draw) addPoints(active)
        renderCanvas()
    }

    let currentSearch = null
    let nextSearch = null
    $addCityPopup.querySelector('input').addEventListener('input', (e) => {
        const name = e.target.value
        const search = () => searchCities(name).then(data => {
            state.foundCities = data.map(c => ({
                name: c.tags.name,
                osmId: c.id
            }))
            currentSearch = nextSearch ? nextSearch() : null
            nextSearch = null
            renderFoundCities()
        })

        if (currentSearch) nextSearch = search
        else currentSearch = search()
    })

    $canvas.addEventListener('mousedown', (e) => {
        state.draw = true
        handleDraw(e)
    })
    $canvas.addEventListener('mouseout', () => {
        state.hoverPoints = []
        renderCanvas()
    })
    document.body.addEventListener('mouseup', () => state.draw = false)
    $canvas.addEventListener('mousemove', handleDraw)
    map.on('resize viewreset zoom move', () => {
        const c = map.getCenter()
        localStorage[state.city + '_clat'] = c.lat
        localStorage[state.city + '_clng'] = c.lng
        loadAndRenderCanvas()
    })
    map.on('zoom', () => localStorage.zoom = map.getZoom() )

    handleClick($modes, 'data-mode', updateMode)
    handleClick($cities, 'data-city', updateCity)
    handleClick($layers, 'data-layer', updateLayer)
    handleClick($labels, 'data-label', updateLabel)
    handleClick($brushes, 'data-brush', updateBrush)

    document.addEventListener('keydown', (e) => {
        if (48 < e.keyCode < 58) {
            const label = labels[e.keyCode - 49]
            if (label) updateLabel(label._id)
        }

        if (e.keyCode === 67)
            updateLabel('remove')

        if (e.keyCode === 80)
            updateMode('paint')

        if (e.keyCode === 73)
            updateMode('inspect')

        if (e.keyCode === 83)
            updateBrush('s')

        if (e.keyCode === 77)
            updateBrush('m')

        if (e.keyCode === 76)
            updateBrush('l')

        if (e.keyCode === 71)
            updateLayer('satellite')

        if (e.keyCode === 78)
            updateLayer('standart')

        if (e.keyCode === 189)
            map.setZoom(map.getZoom() - 1)

        if (e.keyCode === 187)
            map.setZoom(map.getZoom() + 1)

        if (e.keyCode === 37)
            moveMap(-1, 0)

        if (e.keyCode === 38)
            moveMap(0, -1)

        if (e.keyCode === 39)
            moveMap(1, 0)

        if (e.keyCode === 40)
            moveMap(0, 1)
    })

    updateCity(city._id)
    updateLabel(labels[0]._id)
    updateLayer()
    updateMode()
    updateBrush()
    loadAndRenderCanvas(false)
    setInterval(savePoints, 3000)
}

run()
