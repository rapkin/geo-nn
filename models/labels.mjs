import mongoose from 'mongoose'

const schema = mongoose.Schema({
    name: {
        required: true,
        type: String,
        unique: true,
        dropDups: true
    },
    description: {
        type: String,
        required: false
    },
    color: {
        required: true,
        type: String
    }
})

const model = mongoose.model('labels', schema)
export default model
