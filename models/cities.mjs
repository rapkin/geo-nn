import mongoose from 'mongoose'

const schema = mongoose.Schema({
    name: {
        required: true,
        type: String
    },
    osmId: {
        required: true,
        type: Number,
        unique: true,
        dropDups: true
    },
    minlat: {
        required: true,
        type: Number
    },
    minlon: {
        required: true,
        type: Number
    },
    maxlat: {
        required: true,
        type: Number
    },
    maxlon: {
        required: true,
        type: Number
    },
    downloaded: {
        required: false,
        type: Boolean
    }
})

const model = mongoose.model('cities', schema)
export default model
