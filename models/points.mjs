import mongoose from 'mongoose'

export const isCoordinate = (n)  =>
    !isNaN(parseFloat(n)) && isFinite(n) && !(n % 1)

const coordinate = {
    required: true,
    type: Number,
    validate : {
        validator: isCoordinate,
        message: '{VALUE} is not valid coordinate'
    }
}

const schema = mongoose.Schema({
    x: coordinate,
    y: coordinate,
    label: {
        required: true,
        type: mongoose.Schema.Types.ObjectId
    }
})

schema.index({ x: 1, y: 1 }, { unique: true })

const model = mongoose.model('points', schema)
export default model
