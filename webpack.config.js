const path = require('path')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const CssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
    entry: './label-ui/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'label-ui.js'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [ CssExtractPlugin.loader, 'css-loader' ]
            },
            {
                test: /\.(png|jpg|gif|ttf|woff|woff2|eot|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            context: 'node_modules',
                            name: '[path][name].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.html$/,
                use: ['mustache-loader']
            }
        ]
    },
    plugins: [
        new CssExtractPlugin({ filename: 'bundle.css' }),
        new CopyWebpackPlugin([{ from: 'public' }])
    ]
}
