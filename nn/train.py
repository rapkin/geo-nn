import matplotlib
matplotlib.use('Agg')

from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import img_to_array
from keras.utils import to_categorical
from keras.utils import plot_model
from model import Net
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import random
import json
import cv2
import os

DIR = os.path.dirname(os.path.abspath(__file__))
EPOCHS = 400
LR = 1e-3
BS = 32

data = []
labels = []
classes = []

data_dir = os.path.join(DIR, 'data')
samples_dir = os.path.join(DIR, '../data/samples')
classes_file = os.path.join(data_dir, 'classes.json')

if not os.path.exists(data_dir):
    os.makedirs(data_dir)

images = sorted(list(paths.list_images(samples_dir)))
random.seed(42)
random.shuffle(images)

for img in images:
    image = cv2.imread(img)
    image = cv2.resize(image, (128, 128))
    image = img_to_array(image)
    data.append(image)

    label = img.split(os.path.sep)[-2]
    labels.append(label)
    if not label in classes:
        classes.append(label)

classes_count = len(classes)
classes = {name: i for i, name in enumerate(classes)}
labels = [classes[name] for name in labels]

with open(classes_file, 'w') as fp:
    json.dump(classes, fp)

data = np.array(data, dtype='float') / 255.0
labels = np.array(labels)

(trainX, testX, trainY, testY) = train_test_split(data,
    labels, test_size=0.23, random_state=42)

trainY = to_categorical(trainY, num_classes=classes_count)
testY = to_categorical(testY, num_classes=classes_count)

aug = ImageDataGenerator(
    zoom_range=0.4,
    shear_range=0.2,
    rotation_range=50,
    width_shift_range=0.1,
    height_shift_range=0.1,
    horizontal_flip=True,
    fill_mode='nearest'
)

model = Net.build(
    width=128,
    height=128,
    depth=3,
    classes=classes_count
)
opt = Adam(lr=LR, decay=LR / EPOCHS)
model.compile(
    loss='binary_crossentropy',
    optimizer=opt,
    metrics=['accuracy']
)

H = model.fit_generator(
    aug.flow(trainX, trainY, batch_size=BS),
    validation_data=(testX, testY),
    steps_per_epoch=len(trainX) // BS,
    epochs=EPOCHS,
    verbose=1
)

model.save(os.path.join(data_dir, 'network.model'))

plt.style.use('ggplot')

plt.figure()
plt.plot(np.arange(0, EPOCHS), H.history['loss'], label='train_loss')
plt.plot(np.arange(0, EPOCHS), H.history['val_loss'], label='val_loss')
plt.title('Training Loss')
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.legend(loc='lower left')
plt.savefig(os.path.join(data_dir, 'plot_loss.png'))

plt.figure()
plt.plot(np.arange(0, EPOCHS), H.history['acc'], label='train_acc')
plt.plot(np.arange(0, EPOCHS), H.history['val_acc'], label='val_acc')
plt.title('Training Accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.legend(loc='lower left')
plt.savefig(os.path.join(data_dir, 'plot_acc.png'))
