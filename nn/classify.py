from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import argparse
import imutils
import json
import cv2
import os

DIR = os.path.dirname(os.path.abspath(__file__))

ap = argparse.ArgumentParser()
ap.add_argument('-i', '--image', required=True,
    help='path to input image')
args = vars(ap.parse_args())

image = cv2.imread(args['image'])
orig = image.copy()

image = cv2.resize(image, (128, 128))
image = image.astype('float') / 255.0
image = img_to_array(image)
image = np.expand_dims(image, axis=0)

model = load_model(os.path.join(DIR, 'data/network.model'))

values = model.predict(image)[0]
values = ['{0:.2f}'.format(n) for n in values]

with open(os.path.join(DIR, 'data/classes.json')) as file:
    classes = json.load(file)

for key, value in classes.items():
    print(key, values[value])
