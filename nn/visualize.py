from keras.utils import plot_model
from model import Net
import json
import os

DIR = os.path.dirname(os.path.abspath(__file__))

with open(os.path.join(DIR, 'data/classes.json')) as file:
    classes = json.load(file)
classes_count = len(classes.keys())

model = Net.build(width=128, height=128, depth=3, classes=classes_count)

plot_model(model, to_file=os.path.join(DIR, 'data/model.png'), show_shapes=True)
