#!/bin/bash

nameRegex='^([a-z0-9_-]+)'
ipRegex='ansible_ssh_host=(([0-9.]+)|([a-z.]+))'
userRegex='ansible_ssh_user=([a-z0-9_]+)'
portRegex='ansible_ssh_port=([0-9]+)'

notFound= true
while read line; do
    if [[ $line =~ $nameRegex ]]; then
        name=${BASH_REMATCH[1]}
    else
        name=''
    fi

    if [[ $1 ]] && [[ "$1" != "$name" ]]; then
        continue
    fi

    if [[ $line =~ $ipRegex ]]; then
        ip=${BASH_REMATCH[1]}
    else
        ip=''
    fi

    if [[ $line =~ $userRegex ]]; then
        user=${BASH_REMATCH[1]}
    fi

    if [[ $line =~ $portRegex ]]; then
        port=${BASH_REMATCH[1]}
    else
        port=22
    fi

    if [[ $ip ]]; then
        notFound= false
        echo [$name] Copy to $user@$ip:$port
        ssh-copy-id -p $port $user@$ip
    fi
done <hosts

if [[ notFound ]]; then
    echo Server $1 not found
fi
