#!/bin/bash

if [[ $1 ]]; then
    ansible-playbook update.yml -i hosts --limit $1 --ask-sudo-pass
else
    echo You should set group or host to update
fi
