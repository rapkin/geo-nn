#!/bin/bash

if [[ $1 ]]; then
    ansible-playbook install.yml -i hosts --limit $1 --ask-sudo-pass
else
    echo You should set group or host to install
fi
