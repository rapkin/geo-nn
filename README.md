# Territories classification

### Installation

 * Install Python, Node.js and MongoDB
 * Install graphviz (`brew install graphviz`)
 * Clone this repo
 * `npm i` - install deps
 * `npm run reset-db` - clear and fill database with test data
 * `npm run dev` - build project in dev mode
 * `npm start` - start server (default port 2323)
