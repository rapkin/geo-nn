# Модель згорткової нейронної мережі для класифікації зображень

## Математична модель згорткової нейронної мережі

Нехай __N__ - кількість класів, __W, H, D__ - ширина, висота та кількість каналів маски зображення (надалі використовуються з індексами)

__Згортковий шар__ Перший шар мережі повинен бути завжди згортковий. Він реалізує ідею так званих рецептивних полів, тобто набір вхідних нейроні цього шару з’єднаний тільки з певною (невеликою) областю вхідної матриці (наше зображення) і таким чином це моделює деякі особливості людського зору.

Модель, що описує роботу цього шару:

 * На вхід примається __W1 x H1 x D1__ значень.
 * Включає в себе гіперпараметри:
  * кількість фільтрів __K__,
  * їх просторова протяжність __F__,
  * крок __S__,
  * розмір нульових відступів __P__.
 * На виході отримуємо __W2 x H2 x D2__ значень, де
  * __W2 = (W1 - F + 2P) / S + 1__,
  * __H2 = (H1 - F + 2P) / S + 1__ (тобто ширина та висота обчислюються однаково за симетрією),
  * __D2 = K__.
 * При розподілі параметрів цей шар включає __F * F * D1__ ваг на фільтр, а загалом __(F * F * D1) * K__ ваг та __K__ зміщень.
 * На виході зріз глибиною __d__ (розміру __W2 × H2__) є результатом виконання згортки _d_-го фільтра над вхідними значеннями з кроком __S__ та _d_-м зміщенням.

Приклад роботи згорткового шару на рис. 2.1.:
![Рис. 2.1. - Схема подачі значень на вхід згорткового шару](paper/images/conv-example.png)


__Об'єднуючий шар__ Завдання цього шару зменшити розмір вхідної карти значень на якийсь крок (зазвичай проводять змешення у 2 рази за допомогою об'єднуючого шару з ядром 2х2). Кількість каналів залишається незмінною.

Модель, що описує роботу цього шару:

 * На вхід примається __W1 x H1 x D1__ значень.
 * Включає в себе два гіперпараметри:
  * просторова протяжність значень __F__,
  * крок __S__.
 * На виході отримуємо __W2 x H2 x D2__ значень, де
  * __W2 = (W1 - F) / S + 1__,
  * __H2 = (H1 - F) / S + 1__,
  * __D2 = K__.
 * Включає нульові параметри, оскільки він обчислює фіксовану функцію вводу.

__Повністю з'єднаний шар__ Це один із останніх шарів, що дозволяє мережі вчити взаємозв'язки між знайденими характеристиками на етапах згортки. Являє собою шар звичайного перцептрона. Додатково додається (з'єднується з цим шаром) останній шар з розмірністю __N__ яка відповідає кількості класів по яким проводиться класифікація зображення.

Перелік цих шарів можна комбінувати (в залежності від ефекту який ми хочемо отримати) і це лежить в основі створення моделі згорткової мережі. Здійснюється цце зазвичай по наступному шаблону:
```
INPUT -> [[CONV -> RELU]*N -> POOL?]*M -> [FC -> RELU]*K -> FC
```

Таким чином, згорткова мережа перетворює вхідний шар зображення від значень пікселів до остаточних класів. Зауважте, що деякі шари містять параметри, а інші - ні. Зокрема, шари CONV / FC виконують перетворення, що є функцією не тільки активації від вхідних значень, але й параметрів (ваги та зміщення нейронів). З іншого боку, шари RELU / POOL будуть виконувати фіксовану функцію. Параметри в шарах CONV / FC будуть проходити навчання за градієнтним спуском, так що алгоритм оцінює чи обчислення мережі узгоджуються з мітками у навчальному наборі для кожного зображення.

## Алгоритми навчання згорткової нейронної мережі

__Метод зворотного поширення помилки__ (англ. backpropagation) — метод навчання багатошарового перцептрону. Це ітеративний градієнтний алгоритм, який використовується з метою мінімізації помилки роботи багатошарового перцептрону та отримання бажаного виходу. Основна ідея цього методу полягає в поширенні сигналів помилки від виходів мережі до її входів, в напрямку, зворотному прямому поширенню сигналів у звичайному режимі роботи. Для можливості застосування методу зворотного поширення помилки функція активації нейронів повинна бути диференційованою.

Навчання нейронних мереж можна представити як задачу оптимізації. Оцінити — означає вказати кількісно, добре чи погано мережа вирішує поставлені їй завдання. Для цього будується функція оцінки. Вона, як правило, явно залежить від вихідних сигналів мережі і неявно (через функціонування) — від всіх її параметрів. Найпростіший і найпоширеніший приклад оцінки — сума квадратів відстаней від вихідних сигналів мережі до їх необхідних значень:

![де Z* — необхідне значення вихідного сигналу](paper/images/simple-back.png)

Наступний малюнок (рис. 2.2.) підсумовує використання правила ланцюга для зворотного проходження в обчислювальних графах.
![Рис. 2.2. - Перехід вперед ліворуч розраховує z як функцію f(x, y), використовуючи вхідні змінні x та y. Права частина малюнків показує зворотний прохід. Отримавши dL/dz, градієнт функції втрат по z згори, градієнти x і y на функцію втрат можна обчислити, застосовуючи правило ланцюга, як показано на малюнку](paper/images/back-1.png)

На кожній ітерації алгоритму зворотного поширення вагові коефіцієнти нейронної мережі модифікуються так, щоб поліпшити рішення одного прикладу. Таким чином, у процесі навчання циклічно вирішуються однокритеріальні задачі оптимізації.

Навчання нейронної мережі характеризується чотирма специфічними обмеженнями, що виділяють навчання нейромереж із загальних задач оптимізації: астрономічне число параметрів, необхідність високого паралелізму при навчанні, багато критеріально вирішуваних завдань, необхідність знайти досить широку область, в якій значення всіх функцій, що мінімізуються близькі до мінімальних. Стосовно решти проблему навчання можна, як правило, сформулювати як завдання мінімізації оцінки.

__Навчання з підкріпленням__ - це роздiл машинного навчання, що дозволяє реалiзувати заздалегiдь не визначенi конкретно алгоритми, використовуючи формалiзм взаємодiї агенту i оточуючого середовища. На вiдмiну вiд пiдходу навчання з вчителем, процес навчання реалiзується не через прямi вказiвки щодо дiй, а за допомогою системи нагород i штрафiв.
Модель цього алгоритму можна виразити рівнянням на рис. 2.3.

![Рис. 2.3. - Ітераційне рівняння навчання з підкріпленням. Функція Q характеризує винагороду для кожного стану S](paper/images/reinf.png)
