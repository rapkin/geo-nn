const markdownpdf = require('markdown-pdf')
const through = require('through2')
const path = require('path')
const fs = require('fs')

const files = [
    'title.md',
    'contents.md',
    'abstract.md',
    'intro.md',
    'overview.md',
    'convolutional.md',
    'tech.md',
    'implementation.md',
    'labor_protection.md',
    'conclusions.md',
    'resources.md'
]
const out = path.join(__dirname, 'paper.pdf')
const cssPath = path.join(__dirname, 'styles.css')
const runningsPath = path.join(__dirname, 'format.js')

const remarkable = {
    cwd: __dirname,
    html: true,
    plugins: [ require('remarkable-classy') ]
}

const preProcessMd = () => {
    return through((data, enc, cb) => {
        let text = data.toString()
        const matches = text.match(/```[a-z-]+\n\/\/ .*\n```/gm) || []
        for (let match of matches) {
            const [_, lang, file] = match.match(/```([a-z-]+)\n\/\/ (.*)\n```/)
            const code = fs.readFileSync(path.join(__dirname, '..', file))
            text = text.replace(match, ['```'+lang, code, '```'].join('\n'))
        }
        cb(null, Buffer.from(text))
    })
}

const ignore = ['ЗМІСТ', 'ЗАВДАННЯ']
const formatContents = (text) => {
    const matches = text.match(/<h[12].*?>.*?<\/h[12]>/gm)
    let [section, chapter] = [0, 0]
    const contents = matches.map(line => {
        let [full, level, title] = line.match(/<h([12]).*?>(.*?)<\/h[12]>/)
        if (~ignore.indexOf(title)) return ''
        let t = title
        if (level < 2) {
            if (~full.indexOf('base'))
                return `<p>${title}</p>`

            chapter = 0
            section += 1
            t = `РОЗДІЛ ${section}. <br/>${title}`
            title = `РОЗДІЛ ${section}. ${title}`
        }
        else {
            chapter += 1
            t = `${section}.${chapter}. ${title}`
            title = `<span style='margin-left: 10px'>${t}</span>`
        }
        text = text.replace(full, `<h${level}>${t}</h${level}>`)
        return `<p>${title}</p>`
    }).join('')

    const contentsTitle = matches.find(l => ~l.indexOf('contents'))
    return text.replace(contentsTitle, `${contentsTitle} <div class='contents-text'>${contents}</div>`)
}

formatImages = (text) => {
    const matches = text.match(/<img.*?>/gm)
    matches.forEach(line => {
        const [full, title] = line.match(/<img.*?alt="(.+?)".*?>/)
        text = text.replace(full, `${full}<span class='img-label'>${title}</span>`)
    })
    return text
}

const preProcessHtml = () => {
    return through((data, enc, cb) => {
        const text = formatImages(formatContents(data.toString()))
        const styles = "<link rel='stylesheet' href='styles.css'>"
        fs.writeFileSync(path.join(__dirname, 'paper.html'), [styles, text].join(''))
        cb(null, Buffer.from(text))
    })
}

markdownpdf({
    cssPath,
    runningsPath,
    remarkable,
    preProcessHtml,
    preProcessMd,
    paperBorder: {
        top: '1cm',
        right: '2cm',
        bottom: '1.5cm',
        left: '3cm'
    }
})
    .concat.from(files.map(f => path.join(__dirname, f)))
    .to(out, () => console.log('Created', out))
