exports.header = {
    height: '1cm',
    contents: function (page) {
        if (page < 3) return
        return '<p style="text-align:right; text-indent: 0; font-size: 12px">' + page + '</p>'
    }
}

exports.footer = {
    height: '0.5cm',
    contents: function (page) {
        if (page > 1) return
        return '<p style="text-align:center; text-indent: 0">Рівне 2018</p>'
    }
}
