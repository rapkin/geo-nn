# ВИСНОВКИ {base}

Під час дослідження було розроблено модель згорткової нейронної мережі для розпізнавання растрових знімків з супутника з метою класифікації функціональних зон міст.

Наступним кроком стала реалізація розробленої моделі мережі у вигляді програмного коду з використанням бібліотек Keras та Tensorflow на мові програмування Python.

Також було розроблено всі споміжні інструменти для завантаження знімків, їх попередньої обробки та розмітки навчальних даних.

Розроблена модель згорткової штучної нейронної мережі показала задовільні результати під час дослідження ефективності на тестових даних. Показана точність розпізнавання у 93,37% є достатньою для практичного використання. Показана точність може бути покращена шляхом зміни конфігурації деяких елементів мережі, нарощення кількості навчальних данних та реалізації деяких елементів програмного коду на низькорівневій мові.

Отже, в кваліфікаційній роботі:

 * розглянуто доступні підходи до вирішення проблеми класифікації текстур;
 * розглянуто оптимальні джерела картографічної інформації та способи взаємодії з ними;
 * розроблено набір інструментів для обробки та розмітки даних для навчання нейронної мережі;
 * розроблено модель згорткової нейронної мережі для класифікації знімків з супутника;
 * проведено ряд експериментів для підвищення ефективності процесу навчання та подальшої класифікації.
