# СПИСОК ВИКОРИСТАНИХ ДЖЕРЕЛ {base}

 1. [LeCun et al., 1998] Y. LeCun, L. Bottou, Y. Bengio, and P. Haffner. Gradient-based learning applied to document recognition. Proceedings of the IEEE, november 1998., 75 pages
 2. Convolutional Neural Networks (LeNet) - [Електронний ресурс] - Режим доступу: [LeNet5](http://deeplearning.net/tutorial/lenet.html)
 3. Overpass API - [Електронний ресурс] - Режим доступу: [Overpass API](https://wiki.openstreetmap.org/wiki/Overpass_API)
 4. CS231n Convolutional Neural Networks for Visual Recognition - [Електронний ресурс] - Режим доступу: [CS231n Convolutional Neural Networks for Visual Recognition](http://cs231n.github.io/convolutional-networks/#conv)
 5. Mongoose v5.1.6: Documentation - [Електронний ресурс] - Режим доступу: [Mongoose v5.1.6: Getting Started](http://mongoosejs.com/docs/index.html)
 6. Node.js v10.5.0 Documentation - [Електронний ресурс] - Режим доступу: [Node.js v10.5.0 Documentation](https://nodejs.org/dist/latest-v10.x/docs/api/)
 7. Documentation Leaflet - [Електронний ресурс] - Режим доступу: [Documentation Leaflet](https://leafletjs.com/reference-1.3.0.html)
 8. Deconvolution and Checkerboard Artifacts - [Електронний ресурс] - Режим доступу: [Deconvolution and Checkerboard Artifacts](https://distill.pub/2016/deconv-checkerboard/)
 9. Calculus on Computational Graphs: Backpropagation - [Електронний ресурс] - Режим доступу: [Calculus on Computational Graphs: Backpropagation](http://colah.github.io/posts/2015-08-Backprop/)
 10. Conv Nets: A Modular Perspective - [Електронний ресурс] - Режим доступу: [Conv Nets: A Modular Perspective](http://colah.github.io/posts/2015-08-Backprop/)
 11. Conv Nets: A Modular Perspective - [Електронний ресурс] - Режим доступу: [Conv Nets: A Modular Perspective](http://colah.github.io/posts/2014-07-Conv-Nets-Modular/)
 12. Understanding Convolutions - [Електронний ресурс] - Режим доступу: [Understanding Convolutions](http://colah.github.io/posts/2014-07-Understanding-Convolutions/)
 13. Keras Documentation - [Електронний ресурс] - Режим доступу: [Keras Documentation](https://keras.io/)
 14. Tensorflow Python Documentation - [Електронний ресурс] - Режим доступу: [Tensorflow Python Documentation](https://www.tensorflow.org/api_docs/python/)
 15. Python 3.6.6rc1 documentation - [Електронний ресурс] - Режим доступу: [Python 3.6.6rc1 documentation](https://docs.python.org/3.6/index.html)
