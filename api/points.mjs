import Points, { isCoordinate } from '../models/points'
import express from 'express'
import _ from 'lodash'

const router = express.Router()
export default router

router.post('/', async (req, res) => {
    const points = req.body
    for (let { label, x, y } of points)
        if (!label || !isCoordinate(x) || !isCoordinate(y)) {
            const nudes = { error: 'Wrong data!' }
            res.status(400).send(nudes)
            return
        }

    const getName = (p) => p.x + '_' + p.y

    const pairs = points.map(p => ({ x: p.x, y: p.y }))
    const found = {}
    const foundPoints = await Points.find({ $or: pairs })
    foundPoints.forEach(p => found[getName(p)] = p)

    const toSave = points.filter(p => {
        const f = found[getName(p)]
        return f && f.label.toString() !== p.label.toString()
    })
    const toInsert = points.filter(p => !found[getName(p)])

    const inserted = await Points.insertMany(toInsert)
    const tasks = toSave.map(p => async () => {
        const { x, y, label } = p
        const r = await Points.update({ x, y }, { $set: { label } })
        return Object.assign(r, { _id: found[getName(p)]._id })
    })
    const updated = []
    for (let chunk of _.chunk(tasks, 5))
        updated.push(await Promise.all(chunk.map(f => f())))

    res.send({ inserted, updated: _.flatten(updated) })
})

router.get('/clear/:x/:y', async (req, res) => {
    const { x, y } = req.params
    res.send(await Points.deleteOne({ x, y }))
})

router.post('/clear', async (req, res) => {
    const pairs = req.body.map(p => ({ x: p.x, y: p.y }))
    res.send(await Points.deleteMany({ $or: pairs }))
})

router.get('/', async (req, res) => {
    const labels = await Points.find()
    res.send(labels)
})

router.get('/:minx/:maxx/:miny/:maxy', async (req, res) => {
    const { minx, maxx, miny, maxy } = req.params
    const labels = await Points.find({
        x: { $gte: minx*1, $lte: maxx*1 },
        y: { $gte: miny*1, $lte: maxy*1 }
    })
    res.send(labels)
})
