import Labels from '../models/labels'
import express from 'express'

const router = express.Router()
export default router

router.post('/', async (req, res) => {
    const { name, color, _id } = req.body

    if (!name || !color) {
        const nudes = { error: 'Wrong data!' }
        res.status(400).send(nudes)
        return
    }

    let item
    if (_id) {
        item = await Labels.findOne({_id})
        item.name = name
        item.color = color
    } else item = new Labels({ name, color })

    const saved = await item.save()
    res.send(saved)
})

router.get('/', async (req, res) => {
    const labels = await Labels.find()
    res.send(labels)
})
