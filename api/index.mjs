import express from 'express'
import cities from './cities'
import labels from './labels'
import points from './points'

const router = express.Router()
export default router

router.use('/cities/', cities)
router.use('/labels/', labels)
router.use('/points/', points)

router.get('/*', (req, res) =>
    res.status(404).send({ error: 'Api method not found' }))
