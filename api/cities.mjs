import Cities from '../models/cities'
import express from 'express'
import { getCity } from '../helpers/overpass'

const router = express.Router()
export default router

router.put('/', async (req, res) => {
    const osmId = req.body.osmId * 1

    if (!osmId) {
        const nudes = { error: 'Wrong data!' }
        res.status(400).send(nudes)
        return
    }

    const found = await Cities.findOne({ osmId })
    if (found) return res.send(found)

    const city = await getCity(osmId)
    const item = new Cities(Object.assign(city, city.bounds, {
        name: city.tags.name,
        osmId
    }))

    const saved = await item.save()
    res.send(saved)
})

router.get('/', async (req, res) => {
    const cities = await Cities.find()
    res.send(cities)
})
